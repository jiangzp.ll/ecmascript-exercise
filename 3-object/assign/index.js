export default function addSerialNumber(source) {
  // TODO 5: 在这里写实现代码，需要采用ES6 Object.assign
  // eslint-disable-next-line no-unused-vars,no-undef
  const result = Object.assign(source, {
    serialNumber: '12345',
    properties: {
      color: 'green',
      status: 'processed'
    }
  });
  return result;
}
